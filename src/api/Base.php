<?php
/**
 * User: koin@vip.qq.com
 * TimeDate: 2018-11-02 14:35:14
 */

namespace koin\wechat\api;


use think\Cache;

class Base
{
    protected $appid;
    protected $secret;

    public function __construct($appid, $secret)
    {
        $this->appid = $appid;
        $this->secret = $secret;
    }

    /**
     * getAccessToken
     * @author: koin@vip.qq.com
     * @TimeDate: 2018-11-02 14:37:51
     * @throws Exception
     * @return mixed
     */
    public function getAccessToken()
    {
        $token = Cache::get($this->appid . '_token', false);
        if ($token) {
            return $token;
        }

        $url = ApiUrl::ACCESS_TOKEN;
        $param = array(
            'grant_type' => 'client_credential',
            'appid' => $this->appid,
            'secret' => $this->secret,
        );
        $res = $this->sendHttpRequest($url, $param, null, false);
        if (!isset($res['access_token'])) {
            throw new Exception($res['errcode'] . ':' . $res['errmsg'], $res['errcode']);
        }

        Cache::set($this->appid . '_token', $res['access_token'], $res['expires_in'] - 200);
        return $res['access_token'];
    }

    /**
     * sendRequestWithToken
     * @author: koin@vip.qq.com
     * @TimeDate: 2018-11-02 14:38:13
     * @param $url
     * @param null $body_param
     * @param bool $is_post
     * @throws Exception
     * @return mixed
     */
    public function sendRequestWithToken($url, $body_param = null, $is_post = true)
    {
        $token = array(
            'access_token' => $this->getAccessToken()
        );
        return $this->sendHttpRequest($url, $token, $body_param, $is_post);
    }

    /**
     * sendHttpRequest
     * @author: koin@vip.qq.com
     * @TimeDate: 2018-11-02 14:39:08
     * @param $url
     * @param null $url_param
     * @param null $body_param
     * @param bool $is_post
     * @throws Exception
     * @return mixed
     */
    public function sendHttpRequest($url, $url_param = null, $body_param = null, $is_post = true)
    {
        if ($url_param) {
            $url_param = '?' . http_build_query($url_param);
        }
        if ($body_param) {
            $body_param = json_encode($body_param, JSON_UNESCAPED_UNICODE);
        }
        $ch = curl_init($url . $url_param);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($is_post) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body_param);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $data = curl_exec($ch);
        curl_close($ch);
        $array_data = json_decode($data, true);
        if ($array_data) {
            if (isset($array_data['errcode']) && $array_data['errcode'] != 0) {
                throw new Exception($array_data['errcode'] . ':' . $array_data['errmsg'], $array_data['errcode']);
            }
            return $array_data;
        }
        return $data;
    }
}