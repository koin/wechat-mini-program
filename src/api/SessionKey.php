<?php

namespace koin\wechat\api;


class SessionKey extends Base
{
    public function get($code)
    {
        $url = ApiUrl::SESSION_KEY;
        $param = array(
            'appid' => $this->appid,
            'secret' => $this->secret,
            'js_code' => $code,
            'grant_type' => 'authorization_code',
        );
        return file_get_contents($url . '?' . http_build_query($param));
    }
}