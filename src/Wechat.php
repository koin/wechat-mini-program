<?php
/**
 * User: koin@vip.qq.com
 * TimeDate: 2018-11-01 18:25:11
 */

namespace koin\wechat;


use koin\wechat\api\SessionKey;
use think\Cache;

class Wechat
{
    private $appid;
    private $secret;
    private $instance;

    public function __construct($appid, $secret, $token_cache_dir)
    {
        $this->appid = $appid;
        $this->secret = $secret;
        $this->instance = [];
        Cache::init(['path'=>$token_cache_dir]);
    }

    public function getSessionKey($code){
        if(!isset($this->instance['sessionKey'])){
            $this->instance['sessionKey'] = new SessionKey($this->appid,$this->secret);
        }
        return $this->instance['sessionKey']->get($code);
    }
}